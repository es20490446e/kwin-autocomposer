Compatible with any x11 window manager that allows applications to block compositing.

Listed on [Pling](https://www.pling.com/p/1502826).
